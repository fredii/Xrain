#include "rain.h"

// rain properties
float sx = 0.04, sy = 3;
float speed = 5, angle = 0;
float off1 = 0, off2 = 100;
int num_particles = 255;

// rotation
float c, s;
float X1, X2, Y1, Y2;
float v1x, v2x, v3x, v4x, v1y, v2y, v3y, v4y;

// scaling
float xscale, yscale;
#define XS(a) (xscale*(a))
#define YS(a) (yscale*(a))

float rand_float (float a, float b)
{
	float random = ((float) rand()) / (float) RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}

long long GetTickCountMs()
{
	struct timeval te; 
	gettimeofday(&te, NULL);
	long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000;
	return milliseconds;
}

Particle::Particle(float _x, float _y)
{
	x = _x;
	y = _y;
	alpha = rand_float(0.05, 0.1);
}

void Particle::Draw()
{
	glColor4f(1.0f, 1.0f, 1.0f, alpha);
	glVertex2f(XS(v1x + x), YS(v1y + y));
	glVertex2f(XS(v2x + x), YS(v2y + y));
	glVertex2f(XS(v3x + x), YS(v3y + y));
	glVertex2f(XS(v4x + x), YS(v4y + y));
}

void Particle::Tick()
{
	y += speed*c;
	x += speed*s;

	if (y > 110.0f)
	{
		y = -rand_float(10, 40);
		x = rand_float(off1, off2);
	}
}

std::vector <Particle*> particles;
long long ticks = 0, last_ticks = 0;

bool parse_args(int argc, char **argv)
{
	int opt = 0;

	while ((opt = getopt(argc, argv, "s:p:a:o:i:x:y:")) != -1) {
		switch (opt) {
		case 'p':
			num_particles = atoi(optarg);
			break;
		case 's':
			speed = atof(optarg);
			break;
		case 'a':
			angle = DEG2RAD(atof(optarg));
			break;
		case 'o':
			off1 = atof(optarg);
			break;
		case 'i':
			off2 = atof(optarg);
			break;
		case 'x':
			sx = atof(optarg);
			break;
		case 'y':
			sy = atof(optarg);
			break;
		default:
			fprintf(stderr, "Usage: %s [-s rain_speed (def:5.0)] [-p num_particles (def:255)] [-a angle (def 0.0)] [-x x_size (def:0.04)] [-y y_size (def:3)] [-o spawn_left_x (def:0)] [-i spawn_right_x (def:100.0)]\n",
					argv[0]);
			return false;
		}
	}
	return true;
}

void init_effect()
{
	c = cos(angle);
	s = sin(angle);

	X1 = -sx;
	X2 = sx;
	Y1 = -sy;
	Y2 = sy;

	v1x = X1 * c + Y1 * s;
	v1y = X1 * s + Y1 * c;
	v2x = X2 * c + Y1 * s;
	v2y = X2 * s + Y1 * c;
	v3x = X2 * c + Y2 * s;
	v3y = X2 * s + Y2 * c;
	v4x = X1 * c + Y2 * s;
	v4y = X1 * s + Y2 * c;

	for (int i = 0; i < num_particles; i++)
		particles.push_back(new Particle(rand_float(off1, off2), -rand_float(100, 400)));
}

void init_scene(float width, float height)
{
	// set size
	xscale = width / 100.0f;
	yscale = height / 100.0f;

	// opengl stuff
	gluOrtho2D(0, width, height, 0);
	glEnable (GL_BLEND); 
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// timer
	ticks = last_ticks = GetTickCountMs();
}

void draw_scene()
{
	// draw all
	glClearColor(0.0, 0.0, 0.0, 0.0) ;
	glClear(GL_COLOR_BUFFER_BIT) ;
	
	glBegin(GL_QUADS);
	for (size_t i = 0; i < particles.size(); i++)
		particles[i]->Draw();
	glEnd();
}

void process_scene()
{
	// rain physics
	ticks = GetTickCountMs();
	if (last_ticks < ticks - 20)
	{
		for (size_t i = 0; i < particles.size(); i++)
			particles[i]->Tick();

		last_ticks = ticks;
	}
}

void clear_scene()
{
	particles.clear();
}
