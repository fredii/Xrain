#ifndef RAIN_H
#define RAIN_H

#include <vector>

#include <sys/time.h>

#include <math.h>
#define DEG2RAD(a) ((a) * (M_PI/180.0f))

#include <getopt.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

class Particle
{
public:
	Particle(float _x, float _y);
	void Draw();
	void Tick();
private:
	float x, y, alpha;
};

void init_effect();
bool parse_args(int argc, char **argv);
void init_scene(float width, float height);
void draw_scene();
void process_scene();
void clear_scene();

#endif // RAIN_H
